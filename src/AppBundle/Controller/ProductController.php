<?php
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Product;

/**
 * Controller used to manage product contents in the public part of the site.
 *
 * @author Hoàng Trần Thy Ngọc <ngochtt@nal.vn>, <hthyngoc@gmail.com>
 */
class ProductController extends Controller
{
    public $categoryList = [];
    public $unitList = [];

    public function __construct()
    {
        $this->categoryList = [ 'Component', 'Desktop', 'Laptop', 'Networking' ];
        $this->unitList = [ 'Piece', 'Box', 'Tray' ];
    }

    /**
     * @Route("/product/", name="productIndex")
     */
    public function indexAction(Request $request)
    {
        $productName = '';
        $categoryName = '';

        $queryBuilder = $this->getDoctrine()->getRepository(Product::class)->createQueryBuilder('p');

        if ($request->isMethod('POST')) {
            $productName = trim($request->request->get('product'));
            $categoryName = trim($request->request->get('category'));

            if (!empty($categoryName)) {
                $queryBuilder->andWhere('p.category = :category')->setParameter('category', $categoryName);
            }

            if (!empty($productName)) {
                $queryBuilder->andWhere('p.name LIKE :product')->setParameter('product', '%'.$productName.'%');
            }
        }

        $products = $queryBuilder->getQuery()->getResult();

        return $this->render('product/index.html.twig', [
            'products' => $products,
            'categoryList' => $this->categoryList,
            'search' => [
                'productName' => $productName,
                'categoryName' => $categoryName
            ]
        ]);
    }

    /**
     * @Route("/product/create", name="productCreate")
     */
    public function createAction()
    {
        $product = new Product();
        return $this->_edit($product);
    }

    /**
     * @Route("/product/edit/{id}", name="productEdit")
     */
    public function editAction($id)
    {
        $product = $this->getDoctrine()->getRepository(Product::class)->find($id);
        if (empty($product)) {
            return $this->redirectToRoute('productIndex');
        }
        return $this->_edit($product);
    }

    private function _edit(Product $product)
    {
        $request = Request::createFromGlobals();
        $errorsString = '';

        if ($request->isMethod('POST')) {
            $now = new \DateTime();
            $product->setCategory(trim($request->request->get('category')));
            $product->setCode(trim($request->request->get('code')));
            $product->setName(trim($request->request->get('name')));
            $product->setPrice(floatval(trim($request->request->get('price'))));
            $product->setUnit(trim($request->request->get('unit')));
            $product->setQuantityOnHand(floatval(trim($request->request->get('quantityOnHand'))));
            $product->setActive(empty($request->request->get('active')) ? false : true);
            $product->setNote(trim($request->request->get('note')));
            $product->setModified($now);
            if ($product->getId() <=0) {
                $product->setCreated($now);
            }

            $validator = $this->get('validator');
            $errors = $validator->validate($product);
            if (count($errors) > 0) {
                foreach ($errors as $err) {
                    if ($err->getPropertyPath() == 'code') {
                        $errorsString .= 'Product Code: ' . $err->getMessage() . '<br />';
                    } elseif ($err->getPropertyPath() == 'name') {
                        $errorsString .= 'Name: ' . $err->getMessage() . '<br />';
                    } else {
                        $errorsString .= $err->getPropertyPath() . ': ' . $err->getMessage() . '<br />';
                    }
                }
            } else {
                $em = $this->getDoctrine()->getManager();
                $em->persist($product);
                $em->flush();

                return $this->redirectToRoute('productIndex');
            }
        }
        $actionUrl = ($product->getId() <= 0)
            ? $this->generateUrl('productCreate')
            : $this->generateUrl('productEdit', ['id' => $product->getId()]);

        return $this->render('product/edit.html.twig', [
            'errorsString' => $errorsString,
            'product' => $product,
            'actionUrl' => $actionUrl,
            'categoryList' => $this->categoryList,
            'unitList' => $this->unitList
        ]);
    }
}
