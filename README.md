# README #

## Prerequisite: ##
Developer needs to setup a blank Symfony environment first before starting the test. This Symfony environment should be linked to a Mysql database already.
## Maximum allowed time:  1 hour. ##

## Scenario: ##
You are working on a core feature of an Warehouse management system for a computer store in which you need to setup a product master database to allow users to control their product information. You are given the following task to start your work. Once finished, you are required to create a new branch with your name and push all your source code to this git repository.